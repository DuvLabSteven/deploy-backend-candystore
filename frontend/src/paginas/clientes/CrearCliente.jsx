import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const CrearCliente = () => {

    const navigate = useNavigate();

    const [cliente, setCliente] = useState({
        nombre: '',
        edad: '',
        favoritos: '',
        premium: '',
    });

    const { nombre, edad, favoritos, premium } = cliente;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setCliente({
            ...cliente,
            [e.target.name]: e.target.value
        })
    }

    const crearCliente = async () => {
        const data = {
            nombre: cliente.nombre,
            edad: cliente.edad,
            favoritos: cliente.favoritos,
            premium: cliente.premium,

        }

        const response = await APIInvoke.invokePOST(`/api/clientes`, data);
        const idProducto = response._id;

        if (idProducto === '') {
            const msg = "El cliente NO fue creado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate("/admin-clientes");
            const msg = "El cliente fue creado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });

            setCliente({
                nombre: '',
                edad: '',
                favoritos: '',
                premium: '',
            })
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearCliente();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Creación de Clientes"}
                    breadCrumb1={"Listado de clientes"}
                    breadCrumb2={"Creación"}
                    ruta1={"/admin-clientes"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del cliente"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Edad</label>
                                        <input type="text"
                                            className="form-control"
                                            id="edad"
                                            name="edad"
                                            placeholder="Ingrese la edad del cliente"
                                            value={edad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Dulces Favoritos</label>
                                        <input type="text"
                                            className="form-control"
                                            id="favoritos"
                                            name="favoritos"
                                            placeholder="Ingrese los dulces favoritos del cliente"
                                            value={favoritos}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Premium</label>
                                        <input type="text"
                                            className="form-control"
                                            id="premium"
                                            name="premium"
                                            placeholder="Cliente premium (SI o NO)"
                                            value={premium}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>


                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Crear</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default CrearCliente;