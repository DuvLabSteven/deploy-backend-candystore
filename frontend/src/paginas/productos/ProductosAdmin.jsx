import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";

const ProductosAdmin = () => {
    const [productos, setProductos] = useState([]);

    const cargarProductos = async () => {
        const response = await APIInvoke.invokeGET("/api/inventario");

        setProductos(response);
    };

    useEffect(() => {
        cargarProductos();
    }, []);

    //Eliminar productos
    const eliminarProducto = async (e, idProducto) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/inventario/${idProducto}`);

        if (response.msg === 'producto eliminado con exito') {
            const msg = "El Producto fue borrado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarProductos();
        } else {
            const msg = "El producto no fue borrado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }

    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={"Inventario"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Productos"}
                    ruta1={"/home"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title"><Link to={"/productos-crear"} className="btn btn-block btn-primary btn-sm">Crear Producto</Link></h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style={{ width: "10%" }}>Id</th>
                                        <th style={{ width: "15%" }}>Title</th>
                                        <th style={{ width: "10%" }}>Price</th>
                                        <th style={{ width: "10%" }}>Category</th>
                                        <th style={{ width: "10%" }}>Quantity</th>
                                        <th style={{ width: "10%" }}>Description</th>
                                        <th style={{ width: "10%" }}>Options</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {productos.map((item) => (
                                        <tr key={item._id}>
                                            <td>{item._id}</td>
                                            <td>{item.title}</td>
                                            <td>{item.price}</td>
                                            <td>{item.category}</td>
                                            <td>{item.quantity}</td>
                                            <td>{item.description}</td>


                                            <td>
                                                <Link to={`/productos-editar/${item._id}@${item.title}@${item.price}@${item.category}@${item.quantity}${item.description}`} className="btn btn-sm btn-primary">Editar</Link>&nbsp;&nbsp;

                                                <button onClick={(e) => eliminarProducto(e, item._id)} className="btn btn-sm btn-danger">Borrar</button>

                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
};

export default ProductosAdmin;
