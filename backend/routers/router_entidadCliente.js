const express = require("express");
const router = express.Router();

const controladorCliente = require("../controllers/controller_entidadCliente");
router.get("/", controladorCliente.listarClientes);
router.post("/", controladorCliente.crearCliente);
router.put("/:id", controladorCliente.actualizarCliente);
router.delete("/:id", controladorCliente.eliminarCliente);

module.exports = router;
