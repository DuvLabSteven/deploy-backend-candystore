const express = require("express");
const router = express.Router();

const controladorProductos = require("../controllers/controller_inventario");
router.get("/", controladorProductos.listarProductos);
router.get("/:id", controladorProductos.mostrarProducto);
router.post("/", controladorProductos.crearProducto);
router.put("/:id", controladorProductos.actualizarProducto);
router.delete("/:id", controladorProductos.eliminarProducto);

module.exports = router;
