const express = require("express");
const conectarDB = require("./config/conexion");
const cors = require("cors");

const app = express();
const PORT = process.env.PORT || 5000;
conectarDB();
app.use(cors());
//importo la conexion con mongodb
//importo el modelo y las rutas de productos

app.use(express.json());
// app.use(express.urlencoded({ extended: true }));

app.use("/api/inventario", require("./routers/router_inventario"));
app.use("/api/entidadCliente", require("./routers/router_entidadCliente"));
app.use("/api/usuarios", require("./routers/router_usuarios"));
app.use("/api/auth", require("./routers/router_auth"));
//muestra mensaje en el navegador
//Rutas
app.get("/", (req, res) => {
  res.send("Bienvenidos esta configurado su servidor");
});

app.listen(PORT, () => console.log("Prueba - servidor OK! - puerto", PORT));
