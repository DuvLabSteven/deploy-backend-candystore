const modeloProducto = require("../models/model_inventario");
// incluyendo comentarios

/**
 * Metodo para listar registros de la colección http://localhost:8000/api/inventario/listar
 * */
exports.listarProductos = async (req, res) => {
  try {
    const productos = await modeloProducto.find();
    res.json(productos);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

/**
 * Metodo para cargar un producto http://localhost:8000/api/inventario/agregar
 */
exports.crearProducto = async (req, res) => {
  try {
    let producto;
    // creamos nuestro producto
    producto = new modeloProducto(req.body);
    await producto.save();
    res.send(producto);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

/**
 * Metodo para cargar un producto http://localhost:8000/api/inventario/cargar/1
 */
exports.mostrarProducto = async (req, res) => {
  try {
    let producto = await modeloProducto.findById(req.params.id);
    if (!producto) {
      res.status(404).json({ msg: "el producto no existe" });
    }
    res.json(producto);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

/**
 * Metodo para editar un producto
 */
exports.actualizarProducto = async (req, res) => {
  // Haciendo búsqueda por referencia
  try {
    const {
      id,
      title,
      price,
      category,
      quantity,
      iva,
      image,
      description,
      state,
    } = req.body;
    let producto = await modeloProducto.findById(req.params.id);
    if (!producto) {
      res.status(404).json({ msg: "el producto no existe" });
    }
    producto.id = id;
    producto.title = title;
    producto.price = price;
    producto.category = category;
    producto.quantity = quantity;
    producto.iva = iva;
    producto.image = image;
    producto.description = description;
    producto.state = state;

    producto = await modeloProducto.findOneAndUpdate(
      { _id: req.params.id },
      producto,
      { new: true }
    );
    res.json(producto);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

/**
 * Metodo para agregar productos
 */
exports.eliminarProducto = async (req, res) => {
  try {
    let producto = await modeloProducto.findById(req.params.id);
    if (!producto) {
      res.status(404).json({ msg: "el producto no existe" });
    }
    await modeloProducto.findByIdAndRemove({ _id: req.params.id });
    res.json({ msg: "producto eliminado con exito" });
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};
