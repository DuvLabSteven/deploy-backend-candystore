const express = require("express");
const router = express.Router();
const modeloCliente = require("../models/model_entidadCliente");

/**
 * Metodo para listar registros de la colección entidadCliente
 */
exports.listarClientes = async (req, res) => {
  try {
    const clientes = await modeloCliente.find();
    res.json(clientes);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

/*
 * Metodo para agregar clientes http://localhost:8000/api/entidadCliente/agregarCliente
 */
exports.crearCliente = async (req, res) => {
  try {
    let cliente;
    // creamos nuestro cliente
    cliente = new modeloCliente(req.body);
    await cliente.save();
    res.send(cliente);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

/*
 *  * Metodo para editar un producto
 */
exports.actualizarCliente = async (req, res) => {
  try {
    const {
      nombreCliente,
      identificacion,
      tipoIdentificacion,
      direccion,
      tipoCliente,
      correoElectronico,
    } = req.body;
    let cliente = await modeloCliente.findById(req.params.id);
    if (!cliente) {
      res.status(404).json({ msg: "el cliente no existe" });
    }
    cliente.nombreCliente = nombreCliente;
    cliente.identificacion = identificacion;
    cliente.tipoIdentificacion = tipoIdentificacion;
    cliente.direccion = direccion;
    cliente.tipoCliente = tipoCliente;
    cliente.correoElectronico = correoElectronico;

    cliente = await modeloCliente.findOneAndUpdate(
      { _id: req.params.id },
      cliente,
      { new: true }
    );
    res.json(cliente);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.eliminarCliente = async (req, res) => {
  try {
    let cliente = await modeloCliente.findById(req.params.id);
    if (!cliente) {
      res.status(404).json({ msg: "el cliente no existe" });
    }
    await modeloCliente.findByIdAndRemove({ _id: req.params.id });
    res.json({ msg: "cliente eliminado con exito" });
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};
